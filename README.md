# GPS Global  (prueba-backend)

Prueba Backend Desarrollador de software

## Instrucciones

Bienvenido a la prueba backend para el cargo de Desarrollador
de software en GPS Global. El objetivo de esta prueba es conocer
tus habilidades de desarrolo en un proyecto con laravel.

Para desarrollar esta prueba debes seguir las siguientes intrucciones de instalación del proyecto.

### Instalar composer y php 7.3
Depende de tu sistema operativo, debes instalar composer y php 7.3 u php 8 para iniciar este proyecto.

### Instalar Dependencias
```bash
composer install
```

### Iniciar Servidor de desarrollo
```bash
php artisan serve
```

### Ayuda implementación del proyecto en tu equipo
Si tienes problemas para instalar los componentes necesarios podrías visitar esta guía [Instalar php y composer en Ubuntu](https://www.techiediaries.com/install-laravel-8-php-7-3-composer/).


# Desarrollos propuestos

Una vez con el proyecto operando, se te asignaron las siguientes tareas que debes
implementar en esta aplicación, las cuales tomamos en cuenta para analizar tus habilidades,
por lo cuál no hay una forma concreta de hacerlo así que lo dejamos a tu criterio.

Las tareas que debes crear en esta aplicación son las siguientes:

- Obtener datos del recorrido que se encuentran en la clase RecorridoRepository en la función obtenerRegistros() y mostrarlos en el enlace 'http://localhost:xxxx/api/recorrido' (RecorridoController@index).
- Eliminar datos duplicados cuando obtengas el array del recorrido.

Input: 
```bash
[{
"id": 7232602,
"id_gps": "RB0001",
"imei": "868324021743724",
"fecha_hora": "2021-09-30 00:04:49",
"latitud": -33.4757233,
"longitud": -70.697025,
"calle": null,
"datos": "{\"5\": 0, \"88\": 0, \"2\": 0, \"3\": 0, \"405\": 1, \"406\": 1, \"4\": 0, \"30\": 4079, \"29\": 12792, \"65\": 47631146, \"altitude\": 509.0, \"course\": 128.1, \"satelite\": 16, \"speed\": 0, \"hdop\": 0.7, \"idEvent\": 7, \"priority\": 0, \"peer\": [\"178.251.124.45\", \"64802\"]}",
"equipo": "5",
"fecha_servidor": "2021-09-30 00:04:54"
},
{
"id": 7232602,
"id_gps": "RB0001",
"imei": "868324021743724",
"fecha_hora": "2021-09-30 00:04:49",
"latitud": -33.4757233,
"longitud": -70.697025,
"calle": null,
"datos": "{\"5\": 0, \"88\": 0, \"2\": 0, \"3\": 0, \"405\": 1, \"406\": 1, \"4\": 0, \"30\": 4079, \"29\": 12792, \"65\": 47631146, \"altitude\": 509.0, \"course\": 128.1, \"satelite\": 16, \"speed\": 0, \"hdop\": 0.7, \"idEvent\": 7, \"priority\": 0, \"peer\": [\"178.251.124.45\", \"64802\"]}",
"equipo": "5",
"fecha_servidor": "2021-09-30 00:04:54"
}]
```

Output:

```bash
[{
"id": 7232602,
"id_gps": "RB0001",
"imei": "868324021743724",
"fecha_hora": "2021-09-30 00:04:49",
"latitud": -33.4757233,
"longitud": -70.697025,
"calle": null,
"datos": "{\"5\": 0, \"88\": 0, \"2\": 0, \"3\": 0, \"405\": 1, \"406\": 1, \"4\": 0, \"30\": 4079, \"29\": 12792, \"65\": 47631146, \"altitude\": 509.0, \"course\": 128.1, \"satelite\": 16, \"speed\": 0, \"hdop\": 0.7, \"idEvent\": 7, \"priority\": 0, \"peer\": [\"178.251.124.45\", \"64802\"]}",
"equipo": "5",
"fecha_servidor": "2021-09-30 00:04:54"
}]
```

- Modificar el array, debes añadir o usar el indice 'calle' de cada elemento del array que viene null con la dirección,
  para esto usa la función obtenerDireccion(), usando latitud y longitud que vienen en los elementos del array.

Input:

```bash
[{
"id": 7243279,
"id_gps": "RB0001",
"imei": "868324021743724",
"fecha_hora": "2021-09-30 02:04:49",
"latitud": -33.4757233,
"longitud": -70.697025,
"calle": null,
"datos": "{\"5\": 0, \"88\": 0, \"2\": 0, \"3\": 0, \"405\": 1, \"406\": 1, \"4\": 0, \"30\": 4079, \"29\": 12735, \"65\": 47631146, \"altitude\": 506.6, \"course\": 128.1, \"satelite\": 16, \"speed\": 0, \"hdop\": 0.7, \"idEvent\": 7, \"priority\": 0, \"peer\": [\"178.251.124.45\", \"64802\"]}",
"equipo": "5",
"fecha_servidor": "2021-09-30 02:04:53"
}]
```
Output:
```bash
[{
"id": 7243279,
"id_gps": "RB0001",
"imei": "868324021743724",
"fecha_hora": "2021-09-30 02:04:49",
"latitud": -33.4757233,
"longitud": -70.697025,
"calle": 'Avenida los abetos 1111, Providencia, Chile.',
"datos": "{\"5\": 0, \"88\": 0, \"2\": 0, \"3\": 0, \"405\": 1, \"406\": 1, \"4\": 0, \"30\": 4079, \"29\": 12735, \"65\": 47631146, \"altitude\": 506.6, \"course\": 128.1, \"satelite\": 16, \"speed\": 0, \"hdop\": 0.7, \"idEvent\": 7, \"priority\": 0, \"peer\": [\"178.251.124.45\", \"64802\"]}",
"equipo": "5",
"fecha_servidor": "2021-09-30 02:04:53"
}]
```
# Envío del proyecto

Este proyecto debe ser resuelto y comprimido junto con proyecto prueba-frontend en un archivo .zip

Enviar el archivo con asunto Prueba de desarrollo de software a
informatica@gpsglobal.cl

Mucho éxito en este desafio, cualquier consulta preguntar al mismo correo.
