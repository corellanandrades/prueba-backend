<?php

namespace App\Http\Controllers\Api\Recorrido;

use App\Http\Controllers\Controller;
use App\Repositories\DireccionesRepository;
use App\Repositories\RegistrosRepository;
use Illuminate\Http\Request;

class RecorridoController extends Controller
{
    /**
     * Implementar ejercicios propuestos y retornar resultado en esta función.
     *
     */
    public function index()
    {
      return 'Retornar JSON con el recorrido';
    }
}
