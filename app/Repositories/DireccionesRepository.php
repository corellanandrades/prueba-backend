<?php

namespace App\Repositories;

class DireccionesRepository
{
    public function obtenerDireccion($latitud, $longitud): string
    {
        $direccion = 'Dirección no encontrada';

        $peticion = curl_init("https://nominatim.gpsglobal.cl/nominatim/reverse.php?lat={$latitud}&lon={$longitud}&zoom=21&format=json");
        curl_setopt($peticion, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($peticion, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($peticion, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($peticion, CURLOPT_HTTPHEADER, array('Accept: application/json'));

        $resultado = curl_exec($peticion);

        $respuesta_json = json_decode($resultado);

        if (!isset($respuesta_json->error) && isset($respuesta_json->display_name)) {
            $direccion = $respuesta_json->display_name;
        }

        return $direccion;
    }
}
